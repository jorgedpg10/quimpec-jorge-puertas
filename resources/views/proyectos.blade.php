@extends('layouts.app')

@section('title', 'Proyectos')
@section('clase-body', 'home page')

@section('content')

<div class="boxed-container">
    @include('partials.navbar')

    <div class="main-title" style="background-color: #f2f2f2">
        <div class="container">
            <h1 class="main-title__primary">Proyectos</h1>
            <h3 class="main-title__secondary">PROYECTOS REALIZADOS</h3>
        </div>
    </div>
    <div class="breadcrumbs">
        <div class="container">
            <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to BuildPress." href="index.html" class="home">BuildPress</a></span>
            <span typeof="v:Breadcrumb"><span property="v:title">Projects</span></span>
        </div>
    </div>
    <div class="master-container">
        <div class="container">
            <div class="row">
                <main class="col-xs-12" role="main">
                    <div class="row">

                        <div class="portfolio-mini-wrapper portfolio-light">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- GRID WRAPPER FOR CONTAINER SIZING - HERE YOU CAN SET THE CONTAINER SIZE AND CONTAINER SKIN -->
                                        <div class="portfolio-container">
                                            <div id="options" class="text-left">
                                                <!-- THE FILTER BUTTONS -->
                                                <ul id="filters" class="filter">
                                                    <li class="active"><a href="#" data-filter="*">All Projects</a></li>
                                                    <li><a href="#" data-filter=".filter-buildings">Buildings</a></li>
                                                    <li><a href="#" data-filter=".filter-interior-design">Interior Design</a></li>
                                                    <li><a href="#" data-filter=".filter-isolation">Isolation</a></li>
                                                    <li><a href="#" data-filter=".filter-plumbing">Plumbing</a></li>
                                                    <li><a href="#" data-filter=".filter-tiling">Tiling</a></li>
                                                </ul>
                                            </div>

                                            <!-- THE GRID ITSELF WITH ENTRIES -->
                                            <div id="portfolio-content" class="projects-container row">
                                                <div class="project-post filter-buildings col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                    <!-- THE CONTAINER FOR THE MEDIA AND THE COVER EFFECTS -->
                                                    <img src="images/demo/projects/3.jpg" alt="Project Image"  />
                                                    <div class="project-content">
                                                        <div class="inner-project">
                                                            <h3>Contemporary Villa</h3>
                                                            <a href="images/demo/projects/big1.jpg" class="project-link" title="Contemporary Villa" data-rel="prettyPhoto[gallery]">
                                                                Zoom Project
                                                            </a>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="project-post filter-isolation col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                    <!-- THE CONTAINER FOR THE MEDIA AND THE COVER EFFECTS -->
                                                    <img src="images/demo/projects/1.jpg" alt="Project Image" />
                                                    <div class="project-content">
                                                        <div class="inner-project">
                                                            <h3>Green House</h3>
                                                            <a href="projects-single.html" class="project-link">View Project</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="project-post filter-buildings filter-interior-design col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                    <!-- THE CONTAINER FOR THE MEDIA AND THE COVER EFFECTS -->
                                                    <img src="images/demo/projects/4.jpg" alt="Project Image"  />
                                                    <div class="project-content">
                                                        <div class="inner-project">
                                                            <h3>Villa Rustica Renovation</h3>
                                                            <a href="images/demo/projects/big1.jpg" class="project-link" title="Villa Rustica Renovation" data-rel="prettyPhoto[gallery]">
                                                                Zoom Project
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="project-post filter-isolation filter-plumbing col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                    <!-- THE CONTAINER FOR THE MEDIA AND THE COVER EFFECTS -->
                                                    <img src="images/demo/projects/6.jpg" alt="Project Image" />
                                                    <div class="project-content">
                                                        <div class="inner-project">
                                                            <h3>Pool In Luxury Neighbourhood</h3>
                                                            <a href="projects-single.html" class="project-link">View Project</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="project-post filter-buildings filter-tiling col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                    <!-- THE CONTAINER FOR THE MEDIA AND THE COVER EFFECTS -->
                                                    <img src="images/demo/projects/2.jpg" alt="Project Image" />
                                                    <div class="project-content">
                                                        <div class="inner-project">
                                                            <h3>House of Cards</h3>
                                                            <a href="images/demo/projects/big1.jpg" class="project-link" title="House of Cards" data-rel="prettyPhoto[gallery]">
                                                                Zoom Project
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="project-post filter-isolation filter-plumbing col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                    <!-- THE CONTAINER FOR THE MEDIA AND THE COVER EFFECTS -->
                                                    <img src="images/demo/projects/8.jpg" alt="Project Image"  />
                                                    <div class="project-content">
                                                        <div class="inner-project">
                                                            <h3>Green School</h3>
                                                            <a href="projects-single.html" class="project-link">View Project</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="project-post filter-buildings col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                    <img src="images/demo/projects/5.jpg" alt="Project Image"  />
                                                    <div class="project-content">
                                                        <div class="inner-project">
                                                            <h3>House Under The Palms</h3>
                                                            <a href="images/demo/projects/big1.jpg" class="project-link" title="House Under The Palms" data-rel="prettyPhoto[gallery]">
                                                                Zoom Project
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="project-post filter-isolation col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                    <img src="images/demo/projects/7.jpg" alt="Project Image" />
                                                    <div class="project-content">
                                                        <div class="inner-project">
                                                            <h3>Kitchen And Living Room</h3>
                                                            <a href="projects-single.html" class="project-link">View Project</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div><!-- /container -->
    </div>
    @include('partials.footer')
</div><!-- end of .boxed-container -->

@endsection
