<div id="proyectos" class="panel-grid">
    <div class="panel-grid-cell">
        <div class="panel container">
            <h3 class="widget-title">Proyectos</h3>

            <div class="container" role="main">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel widget widget_pt_featured_page panel-first-child panel-last-child"
                             id="panel-7-1-0-0">
                            <div class="has-post-thumbnail page-box page-box--block">
                                    <img class="imagen-servicios1" src="{{ asset('images/proyectos/el-recreo.jpg') }}"
                                         alt="Content Image"/>
                                <div class="page-box__content">
                                    <h5 class="page-box__title  text-uppercase">
                                        CENTRO COMERCIAL EL RECREO
                                    </h5>
                                    Proyecto realizado en el centro comercial El Recreo, QUito-Ecuador

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel widget widget_pt_featured_page panel-first-child panel-last-child"
                             id="panel-7-1-1-0">
                            <div class="has-post-thumbnail page-box page-box--block">

                                    <img class="imagen-servicios2" src="{{ asset('images/proyectos/lo-nuestro.jpg') }}"
                                         alt="Content Image"/>

                                <div class="page-box__content">
                                    <h5 class="page-box__title text-uppercase">
                                        RESTAURANTE LO NUESTRO
                                    </h5>
                                    Proyecto en el Restaurante Lo Nuestro

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row" style="padding-top: 50px;">
                    <div class="col-md-6">
                        <div class="panel widget widget_pt_featured_page panel-first-child panel-last-child"
                             id="panel-7-1-0-0">
                            <div class="has-post-thumbnail page-box page-box--block">
                                <img class="imagen-servicios1" src="{{ asset('images/proyectos/iglesia.jpg') }}"
                                     alt="Content Image"/>
                                <div class="page-box__content">
                                    <h5 class="page-box__title  text-uppercase">
                                        Iglesia en Otavalo
                                    </h5>
                                    Iglesia con techo acústico flexlining en la ciudad de Otavalo -Ecuador

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel widget widget_pt_featured_page panel-first-child panel-last-child"
                             id="panel-7-1-1-0">
                            <div class="has-post-thumbnail page-box page-box--block">

                                <img class="imagen-servicios2" src="{{ asset('images/proyectos/centro-comercial.jpg') }}"
                                     alt="Content Image"/>

                                <div class="page-box__content">
                                    <h5 class="page-box__title text-uppercase">
                                        CENTRO COMERCIAL
                                    </h5>
                                    Centro comercial Techo metálico tipo teja con gravilla tabaco en Amaguaña –Quito.

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
