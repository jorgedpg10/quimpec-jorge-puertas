<footer>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <div class="widget  widget_text  push-down-30">
                        <h6 class="footer__headings">SOBRE NOSOTROS</h6>
                        <div class="textwidget">
                            <img src="{{ asset('images/logo.png') }}" alt="logo quimpec" width="218" height="45"/>
                            <br><br>
                            Fabricamos productos para cubrir las necesidades en varias áreas de la industria.
                            <br><br>
                            <strong><a href="{{ route('index-scroll.vista', 'quienes-somos') }}" class="read-more">MÁS</a></strong>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12  col-md-4">
                    <div class="widget  widget_nav_menu  push-down-30">
                        <h6 class="footer__headings">MAPA DEL SITIO</h6>
                        <div class="menu-top-menu-container">
                            <ul id="menu-top-menu-1" class="menu">
                                <li><a href="{{ route('index') }}">Home</a>
                                <li><a href="{{ route('categorias') }}">Catálogo</a>
                                    <ul class="sub-menu">
                                        <li><a href="{{ route('construccion.index') }}">Construcción</a></li>
                                        <li><a href="{{ route('subcategorias-didactico') }}">Productos Didácticos y Artísticos</a>
                                            <ul class="sub-menu">
                                                <li><a href="{{ route('categoria.index','escolar') }}">Escolar</a></li>
                                                <li><a href="{{ route('categoria.index','manualidades') }}">Manualidades</a>
                                                <li><a href="{{ route('categoria.index','fantasia') }}">Maquillaje de Fantasía</a>
                                            </ul>
                                        </li>
                                        <li><a href="{{ route('limpieza.index') }}">Limpieza y desinfección</a></li>
                                    </ul>
                                <li><a href="{{ route('blog') }}">Blog</a>
                                <li><a href="{{ route('contacto') }}">Contacto</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12  col-md-4">
                    <div class="widget  widget_text  push-down-30">
                        <h6 class="footer__headings">CONTACTO</h6>
                        <div class="textwidget">
                            <p>
                                Sector Parque Industrial de Turubamba<br>
                                Calle A Lote 37 y Calle J
                            </p>

                            <p>
                                <br>
                                +593 02 2975570
                                <br>
                                ventas@quimpec.com

                            </p>
                            <br>
                            <br>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="footer-bottom__left">
                Buenas ideas...Buena Química...Buenos Productos
            </div>
            <div class="footer-bottom__right">
                &copy;<strong>Quimpec Químicas Cia. Ltda.</strong> Todos los derechos reservados.
            </div>
            <a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
        </div>
    </div>
</footer>
