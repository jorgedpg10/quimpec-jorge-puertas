<div class="siteorigin-panels-stretch panel-row-style testimonials-bg">
    <div class="container">
        <div class="panel-grid">
            <div class="panel-grid-cell" id="pgc-7-4-0">
                <div class="panel widget widget_pt_testimonials panel-first-child panel-last-child">
                    <div class="testimonial">
                        <h2 class="widget-title">
                            <a class="testimonial__carousel testimonial__carousel--left" href="#carousel-testimonials-widget-4-0-0" data-slide="next">
                                <i class="fa  fa-angle-right" aria-hidden="true"></i>
                                <span class="sr-only" role="button">Next</span>
                            </a>
                            <a class="testimonial__carousel  testimonial__carousel--right" href="#carousel-testimonials-widget-4-0-0" data-slide="prev">
                                <i class="fa  fa-angle-left" aria-hidden="true"></i>
                                <span class="sr-only" role="button">Previous</span>
                            </a>
                            Testimonios
                        </h2>
                        <div id="carousel-testimonials-widget-4-0-0" class="carousel slide">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <div class="row">
                                        <div class="col-xs-12  col-sm-6">
                                            <blockquote class="testimonial__quote">
                                                Quimpec nos ha proveído cerca de 800.000 metros cuadrados.
                                                Demostrando su compromiso y cumplimiento del trabajo contratado
                                                a satisfacción del cliente y calidad requerida
                                            </blockquote>
                                            <br>
                                            <cite class="testimonial__author">Ing. Ramiro Garzón, Gerente General</cite>
                                            <br>
                                            <br>

                                            <img width="25%" src="{{ asset('images/marcas/novacero.png') }}" alt="logo novacero">
                                        </div>
                                        <div class="col-xs-12  col-sm-6">
                                            <blockquote class="testimonial__quote">
                                                Quimpec nos provee el servicio de pintura termoacústica desde el año 2019, dentro de
                                                los plazos y requerimientos establecidos.
                                            </blockquote>
                                            <cite class="testimonial__author">Ing. Olivia Díaz, Gerente de adquisiciones</cite>
                                            <br>
                                            <br>
                                            <img width="25%" src="{{ asset('images/marcas/kubiec.png') }}" alt="logo kubiec">
                                        </div>

                                    </div>
                                </div>
                                <div class="item">
                                    <div class="row">
                                        <div class="col-xs-12  col-sm-6">
                                            <blockquote class="testimonial__quote">
                                                Como resultado de su innovación e investigación tecnológica Quimpec
                                                ha desarrollado un techo acústico, una solución única en su género a nivel nacional
                                            </blockquote>
                                            <cite class="testimonial__author">Ing. Ramiro Garzón, Gerente General</cite>
                                            <br>
                                            <br>
                                            <img width="25%" src="{{ asset('images/marcas/novacero.png') }}" alt="logo novacero">
                                        </div>

                                        <div class="col-xs-12  col-sm-6">
                                            <blockquote class="testimonial__quote">
                                                Utilizamos el techo provisto por la empresa Quimpec Químicas en la base de operaciones
                                                de la Compañía Schlumberger Surenco S.A. presentando excelentes resultados como aisalamiento termo-acústico en galpones
                                                de gran altura.
                                            </blockquote>
                                            <cite class="testimonial__author">Ing. Diego Proaño Cevallos, Gerente de Proyecto</cite>
                                            <br>
                                            <br>
                                            <img width="25%" src="{{ asset('images/marcas/citycare2.jpg') }}" alt="logo citycare">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
