<div class="jumbotron  jumbotron--with-captions">
    <div class="carousel  slide  js-jumbotron-slider" id="headerCarousel" data-interval="3000">
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="{{ asset('images/slider/bodypaint-hero-90.jpg') }}" alt="body paint">
                <div class="container">
                    <div class="carousel-content">
                        <div class="jumbotron__category">
                            <h6>ARTÍSTICOS</h6>
                        </div>
                        <div class="jumbotron__title">
                            <h1>Maquillaje de Fantasía</h1>
                        </div>
                        <div class="jumbotron__content">
                            <p>Productos para que tu fiesta sea inolvidable</p>
                            <a class="btn  btn-primary" href="{{ route('categoria.index', 'fantasia') }}" target="_self">VER MÁS</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="{{ asset('images/slider/casa.jpg') }}" alt="Materiales de construcción de última generación" class="slider-1" />
                <div class="container">
                    <div class="carousel-content">
                        <div class="jumbotron__category">
                            <h6>CONSTRUCCIÓN</h6>
                        </div>
                        <div class="jumbotron__title">
                            <h1>Productos Para la Construcción</h1>
                        </div>
                        <div class="jumbotron__content">
                            <p>Materiales de construcción de última generación, basado en nano tecnología.</p>
                            <a class="btn  btn-primary" href="{{ route('construccion.index') }}" target="_blank">VER MÁS</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="{{ asset('images/slider/munheco.jpg') }}" alt="ddeditos">
                <div class="container">
                    <div class="carousel-content">
                        <div class="jumbotron__category">
                            <h6>DIDÁCTICOS</h6>
                        </div>
                        <div class="jumbotron__title">
                            <h1>Productos Didácticos</h1>
                        </div>
                        <div class="jumbotron__content">
                            <p>Productos para manualidades y escolares</p>
                            <a class="btn  btn-primary" href="{{ route('categoria.index', 'escolar') }}" target="_self">VER MÁS</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item">
                <img src="{{ asset('images/slider/alcohol.jpg') }}" alt="alcohol limpieza jabón" />
                <div class="container">
                    <div class="carousel-content">
                        <div class="jumbotron__category">
                            <h6>SALUD</h6>
                        </div>
                        <div class="jumbotron__title">
                            <h1>Limpieza Y Desinfección</h1>
                        </div>
                        <div class="jumbotron__content">
                            <p>Los mejores productos para cuidar la salud de los Ecuatorianos</p>
                            <a class="btn  btn-primary" href="{{ route('limpieza.index') }}" target="_self">VER MÁS</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#headerCarousel" role="button" data-slide="prev">
            <i class="fa fa-angle-left"></i>
        </a>
        <a class="right carousel-control" href="#headerCarousel" role="button" data-slide="next">
            <i class="fa fa-angle-right"></i>
        </a>
    </div>
</div>
