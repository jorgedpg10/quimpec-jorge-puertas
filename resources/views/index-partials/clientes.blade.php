<div class="panel-grid">
    <div class="panel-grid-cell">
        <div class="panel container">
            <h3 class="widget-title">Nuestras Marcas</h3>
            <div class="textwidget">
                <div class="logo-panel">
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="col-xs-12 col-sm-2"><img src="{{ asset('images/marcas/deditos-final.jpg') }}" alt="logo deditos" width="208" height="98"></div>
                        <div class="col-xs-12 col-sm-2"><img src=" {{ asset('images/marcas/bedore-final.jpg') }}" alt="logo bedore" width="208" height="98"></div>
                        <div class="col-xs-12 col-sm-2"><img src="{{ asset('images/marcas/aquaprimer.png') }}" alt="logo aquaprimer" width="208" height="98"></div>
                        <div class="col-xs-12 col-sm-2"><img src="{{ asset('images/marcas/flexlining.png') }}" alt="logo flexlining" width="208" height="98"></div>
                        {{--<div class="col-xs-12 col-sm-2"><img src="{{ asset('images/marcas/tegal.png') }}" alt="Client" width="208" height="98"></div>--}}
                        <div class="col-xs-12 col-sm-2"><img src="{{ asset('images/marcas/floralina.png') }}" alt="logo floralina" width="208" height="98"></div>
                        <div class="col-sm-1"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
