<div id="quienes-somos" class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="panel widget widget_text panel-first-child" id="panel-7-3-1-0">
                <div class="textwidget"></div>
            </div>
            <div class="panel panel-grid widget widget_black-studio-tinymce panel-last-child" id="panel-7-3-1-1">
                <h3 class="widget-title">Quiénes Somos?</h3>
                <div class="textwidget">
                    <p>
                            <img class="alignleft wp-image-115 size-medium" src="{{ asset('images/quienes-somos/laboratorio.jpg') }}" alt="fábrica quimpec" width="300" height="168"/>

                            <img class="alignleft wp-image-116 size-medium" src="{{ asset('images/quienes-somos/tarros.jpg') }}" alt="fábrica pinturas" width="300" height="168"/>

                    </p>
                    <p>
                        Quimpec nace en Quito, Ecuador en el año 1993 con el objetivo de fabricar productos para cubrir las
                        necesidades en varias áreas de la industria y ofrecer productos manufacturados con elevado valor
                        agregado.
                    </p>
                    <p>Durante este tiempo hemos sumado a nuestro portafolio varios productos que nos permiten
                        satisfacer la demanda de nuestros clientes y de la sociedad en general.</p>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel widget widget_text panel-first-child" id="panel-7-3-0-0">
                <div class="textwidget"></div>
            </div>
            <div class="panel panel-grid widget widget_black-studio-tinymce panel-last-child" id="panel-7-3-0-1">
                <h3 class="widget-title">Beneficios</h3>
                <div class="textwidget">
                    <h5>
                        <span style="color: #fcc71f"><br/><span class="icon-container"><span class="fa fa-check"></span></span></span>
                        <span style="color: #333333">EXPERIENCIA</span>
                    </h5>
                    <p>
                        27 años en la industria, generando empleo y cumpliendo con los requerimientos de los Ecuatorianos
                    </p>
                    <h5>
                        <span style="color: #fcc71f"><br/><span class="icon-container"><span class="fa fa-check"></span></span></span>
                        INTEGRIDAD
                    </h5>
                    <p>
                        Nuestros productos satisfacen las necesidades de los usuarios, somos creíbles y generamos confianza
                    </p>
                    <h5>
                        <span style="color: #fcc71f"><br/><span class="icon-container"><span class="fa fa-check"></span></span></span>
                        <span style="color: #333333">COMPROMISO</span>
                    </h5>
                    <p>
                        Rápida capacidad de respuesta y servicio. Los clientes son nuestra razón de ser
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
