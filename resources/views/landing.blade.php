@extends('layouts.app')

@section('title', 'Proyectos')
@section('clase-body', 'home page')
@section('extra-css')
    <link rel="stylesheet" href="{{ asset('css/landing.css') }}"/>
    <link href="https://vjs.zencdn.net/7.14.3/video-js.css" rel="stylesheet"/>
    <script src="https://vjs.zencdn.net/7.14.3/video.min.js"></script>
    <script src="{{ asset('js/videojs-playlist.min.js') }}"></script>
@endsection

@section('content')
    @include('partials.navbar')

    <div class="main-title" style="background-color: #f2f2f2; ">
    </div>

    <div class="master-container">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="text-center hentry__title titulo">Laca de cabello</h3>
                </div>

            </div>

            <div class="row">
                <div class="col-xs-12">
                    {{--<video width="100%" controls>
                        <source src=" {{asset('videos/tinte-cabello.mp4')}} " type="video/mp4">
                        Your browser does not support this video.
                    </video>--}}

                    <video
                        id="video-fantasia"
                        class="video-js vjs-fluid"
                        controls
                        preload="auto"
                        data-setup="{}"
                    >

                        <p class="vjs-no-js">
                            To view this video please enable JavaScript, and consider upgrading to a
                            web browser that
                            <a href="https://videojs.com/html5-video-support/" target="_blank"
                            >supports HTML5 video</a
                            >
                        </p>
                    </video>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 text-center padding-boton">
                    <a href=" {{ route('categorias') }}">
                        <button class="btn btn-primary" type="button">Explora más productos</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('extra-js')



    <script>
        var player = videojs('video-fantasia');
        var video1 = {!!  json_encode( asset('videos/tinte-cabello.mp4')) !!} ;
        var video2 = {!!  json_encode( asset('videos/pintura-acrilica-unas.mp4')) !!} ;
        var video3 = {!!  json_encode( asset('videos/crema-colores-deditos.mp4')) !!} ;
        var video4 = {!!  json_encode( asset('videos/latex-sangre.mp4')) !!} ;

        player.playlist([{
            sources: [{
                src: video1,
                type: 'video/mp4'
            }],
        }, {
            sources: [{
                src: video2,
                type: 'video/mp4'
            }],
            poster: 'http://media.w3.org/2010/05/bunny/poster.png'
        }, {
            sources: [{
                src: video3,
                type: 'video/mp4'
            }],

        }, {
            sources: [{
                src: video4,
                type: 'video/mp4'
            }],

        }]);

        player.playlist.autoadvance(0);
    </script>
@endsection
