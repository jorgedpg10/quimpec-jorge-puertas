@extends('layouts.app')

@section('title', 'Catálogo - Limpieza')

@section('seo')
    <meta name="title" content="Catálogo de Productos Limpieza"/>
    <meta name="description" content="Catálogo línea de Limpieza. Como alcohol en Gel y jabón lpiquido
     . Hechos en Ecuador. Entra y conoce más." />
    <meta name="keywords" content="Jabón Líquido, alcohol en gel, Quito, Ecuador" />

@endsection

@section('extra-css')
    <link rel="stylesheet" href="{{ asset('css/shop.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/productos2.css') }}"/>
@endsection

@section('clase-body', 'woocommerce-page')
@section('content')

    <div class="boxed-container">
        @include('partials.navbar')

        <div class="main-title">
            <div class="container">
                <h1 class="main-title__primary">Catálogo</h1>
                <h3 class="main-title__secondary">NUESTROS PRODUCTOS</h3>
            </div>
        </div>
        <div class="breadcrumbs ">
            <div class="container">
                <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Ir al inicio"
                                               href="{{ route('index') }}" class="home">Quimpec</a></span>
                <span typeof="v:Breadcrumb"><a title="ir al Catálogo" href="{{ route('categorias') }}">Catálogo</a></span>
                <span property="v:title">Limpieza y Desinfección</span>
            </div>
        </div>
        <div class="master-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12  col-md-9  col-md-push-3" role="main">
                        <div class="clearfix"></div>

                        <div class="grid-container">
                            @foreach($productos as $producto)
                                <div class="item">
                                    <a href="{{ route('producto-limpieza.show', $producto->id) }}">
                                        <img width="100%"
                                             src="{{ asset('/storage/'.$producto->imagen_portada)  }}"
                                             alt="Forceps"/>
                                        <h3>{{ $producto->nombre }}</h3>

                                    </a>
                                </div>
                            @endforeach
                        </div>

                    </div>
                    <div class="col-xs-12  col-md-3  col-md-pull-9">
                        @include('productos.partials.busqueda-categorias')
                    </div>
                </div>
            </div>
        </div>
        @include('index-partials.footer')
    </div>

@endsection
