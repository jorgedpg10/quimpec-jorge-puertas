@extends('layouts.app')

@section('title', 'Catálogo')

@section('seo')
    <meta name="title" content="Catálogo de Productos - Quimpec Químicas Quito Ecuador"/>
    <meta name="description" content="Catálogo línea de construcción, Productos didácticos y Artísticos, línea de limpieza
y desinfección " />
    <meta name="keywords" content="Techos acústicos, Pintura acústica, Pintura para cara, Body paint,
Impermeabilizante de humedad, Jabón líquido y alcohol en gel, Limpiavidrios y limpia tapiceria, Pintura para tela,
plastilinas, crayones, temperas, arcilla, masa suave, Maquillaje de fantasía" />

@endsection

@section('extra-css')
    <link rel="stylesheet" href="{{ asset('css/shop.css') }}"/>
    <link rel="stylesheet" href=" {{ asset('css/categorias.css') }}"/>
@endsection

@section('clase-body', 'woocommerce-page')
@section('content')

    <div class="boxed-container">
        @include('partials.navbar')

        <div class="main-title">
            <div class="container">
                <h1 class="main-title__primary">Catálogo</h1>
                <h3 class="main-title__secondary">NUESTROS PRODUCTOS</h3>
            </div>
        </div>
        <div class="breadcrumbs ">
            <div class="container">
                <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to BuildPress." href="{{ route('index') }}" class="home">Quimpec</a></span>
                <span property="v:title">Catálogo</span>
            </div>
        </div>

        <div class="master-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12  col-md-9  col-md-push-3" role="main">
                        <div class="grid-container">
                            <div class="item">
                                <a href="{{ route('construccion.index') }}">
                                    <img width="100%"
                                         src=" {{ asset('images/lineas-productos/acabado.jpg') }}"
                                         alt="línea construcción"/>
                                    <h3>Construcción</h3>
                                </a>
                            </div>

                            <div class="item">
                                <a href="{{ route('subcategorias-didactico') }}">
                                    <img width="100%"
                                         src=" {{ asset('images/lineas-productos/arte-escolar.jpg') }}"
                                         alt="línea didacticos"/>
                                    <h3>Productos Didácticos y Artísticos</h3>
                                </a>
                            </div>

                            <div class="item">
                                <a href="{{ route('limpieza.index') }}">
                                    <img width="100%"
                                         src=" {{ asset('images/lineas-productos/limpieza2.jpg') }}"
                                         alt="línea limpieza"/>
                                    <h3>Limpieza Y Desinfección</h3>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12  col-md-3  col-md-pull-9">
                        @include('productos.partials.busqueda-categorias')
                    </div>
                </div>
            </div>
        </div>
        @include('index-partials.footer')
    </div>

@endsection
