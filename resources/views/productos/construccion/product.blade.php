@extends('layouts.app')
@section('title',$producto->nombre)

@section('extra-css')
    <link rel="stylesheet" href="{{ asset('css/shop.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/splide/splide.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/pinta-dedos.css') }}"/>
@endsection

@section('clase-body', 'woocommerce-page')
@section('content')

    <div id="arreglo-imagenes" style="display: none;">
        {{ $producto->imagenes }}
    </div>
    <div class="boxed-container">
        @include('partials.navbar')
        <div class="main-title">
            <div class="container">
                <h1 class="main-title__primary">Catálogo</h1>
                <h3 class="main-title__secondary">Nuestros Productos</h3>
            </div>
        </div>
        <div class="breadcrumbs ">
            <div class="container">
                <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Ir al inicio."
                                               href="{{ route('index') }}" class="home">Quimpec</a></span>
                <span typeof="v:Breadcrumb"><a title="ir al Catálogo" href="{{ route('categorias') }}">Catálogo</a></span>
                <span typeof="v:Breadcrumb"><a title="Go to Shop." href="{{ route('construccion.index') }}">Construcción</a></span>
                <span property="v:title">{{ $producto->nombre }}</span>
            </div>
        </div>
        <div class="master-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12  col-md-9  col-md-push-3" role="main">
                        <div class="product product-type-simple">
                            <div class="images">

                                <div id="primary-slider" class="splide">
                                    <div class="splide__track" >
                                        <ul class="splide__list">
                                            @foreach($arreglo_json as $imagen)
                                                <li class="splide__slide" >
                                                    <img src="{{ asset('/storage/'.$imagen) }} ">
                                                </li>
                                            @endforeach

                                        </ul>
                                    </div>
                                </div>

                                <div id="secondary-slider" class="splide">
                                    <div class="splide__track">
                                        <ul class="splide__list">
                                            @foreach($arreglo_json as $imagen)
                                                <li class="splide__slide">
                                                    <img src="{{ asset('/storage/'.$imagen) }}">
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                            </div>
                            <div class="summary entry-summary">
                                <div class="woocommerce-product-rating"></div>
                                <h1 class="product_title entry-title">{{ $producto->nombre }}</h1>
                                <div>

                                </div>
                                <div class="short-description" style="color: #000000;">
                                    <p>
                                      {!! $producto->texto1 !!}
                                    </p>
                                </div>

                                <div class="product_meta">
                                    <span class="posted_in">Categoría <a href="{{ route('construccion.index') }}">Construcción</a>.</span>
                                </div>
                            </div><!-- .summary -->
                            <div class="woocommerce-tabs">
                                <div class="panel entry-content" id="tab-description">
                                    <h2></h2>
                                    <p>
                                        {!! $producto->texto2 !!}
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12  col-md-3  col-md-pull-9">
                        <div class="sidebar shop-sidebar">
                            @include('productos.partials.busqueda-categorias')

                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('index-partials.footer')
    </div>
@endsection

@section('extra-js')
    <script src="{{ asset('js/splide.min.js')}}"></script>

    <script>
        // Create and mount the thumbnails slider.
        var secondarySlider = new Splide( '#secondary-slider', {
            rewind      : true,
            fixedWidth  : 100,
            fixedHeight : 100,
            isNavigation: true,
            gap         : 10,
            focus       : 'center',
            pagination  : false,
            cover       : true,
            breakpoints : {
                '600': {
                    fixedWidth  : 66,
                    fixedHeight : 40,
                }
            }
        } ).mount();

        // Create the main slider.
        var primarySlider = new Splide( '#primary-slider', {
            type       : 'fade',
            heightRatio: 1,
            pagination : false,
            arrows     : false,
            cover      : true,

        } );

        // Set the thumbnails slider as a sync target and then call mount.
        primarySlider.sync( secondarySlider ).mount();

    </script>
@endsection


