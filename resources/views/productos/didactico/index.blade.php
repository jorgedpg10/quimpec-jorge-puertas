@extends('layouts.app')

@section('title', 'Catálogo ')


@section('seo')
    <meta name="title" content="Catálogo de Productos Didácticos y Artísticos"/>
    <meta name="description" content="Catálogo línea de productos Didácticos y Artísticos. Como
plastilina, crayones, pintura para el cuerpo, Maquillaje de Fantasía, témperas. Hechos en Ecuador
Entra y conoce más." />
    <meta name="keywords" content="Plastilina, crayones, bodypaint, body paint, pintura para el cuerpo,
pintura para fiesta, maquillaje de fantasía, pintura de payaso" />

@endsection

@section('clase-body', 'woocommerce-page')
@section('extra-css')
    <link rel="stylesheet" href="{{ asset('css/shop.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/productos2.css') }}"/>
@endsection

@section('content')

    <div class="boxed-container">
        @include('partials.navbar')

        <div class="main-title">
            <div class="container">
                <h1 class="main-title__primary">Catálogo</h1>
                <h3 class="main-title__secondary">NUESTROS PRODUCTOS</h3>
            </div>
        </div>
        <div class="breadcrumbs ">
            <div class="container">
                <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to BuildPress."
                                               href="{{ route('index') }}" class="home">Quimpec</a></span>
                <span typeof="v:Breadcrumb"><a title="ir al Catálogo" href="{{ route('categorias') }}">Catálogo</a></span>
                <span property="v:title">Escolar y Artísticos</span>
            </div>
        </div>
        <div class="master-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12  col-md-9  col-md-push-3" role="main">
                        <div class="clearfix"></div>

                        <div class="grid-container">
                            @foreach($productos as $producto)
                                <div class="item">
                                    <a href="{{ route('producto.show', $producto->id) }}">
                                        <img width="100%"
                                             src="{{ asset('/storage/'.$producto->imagen_portada)  }}"
                                             alt="producto didactico"/>
                                        <h3>{{ $producto->nombre }}</h3>

                                    </a>
                                </div>
                            @endforeach
                        </div>

                    </div>

                    <div class="col-xs-12  col-md-3  col-md-pull-9">
                        @include('productos.partials.busqueda-categorias')
                    </div>
                </div>
            </div>
        </div>
        @include('index-partials.footer')
    </div>

@endsection
