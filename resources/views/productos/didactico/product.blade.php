@extends('layouts.app')

@section('title', $producto->nombre)

@section('extra-css')
    <link rel="stylesheet" href="{{ asset('css/shop.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/splide/splide.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/productos-didactico.css') }}"/>
@endsection

@section('clase-body', 'woocommerce-page')
@section('content')
    <div class="boxed-container">
        @include('partials.navbar')
        <div class="main-title">
            <div class="container">
                <h1 class="main-title__primary">Catálogo</h1>
                <h3 class="main-title__secondary">Nuestros Productos</h3>
            </div>
        </div>
        <div class="breadcrumbs ">
            <div class="container">
                <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to BuildPress."
                                               href="{{route('index')}}" class="home">Quimpec</a></span>
                <span typeof="v:Breadcrumb"><a title="ir al Catálogo" href="{{ route('categorias') }}">Catálogo</a></span>
                <span typeof="v:Breadcrumb"><a title="Go to Shop." href="{{ route('categoria.index', 'escolar') }}">Escolar</a></span>

                <span property="v:title">{{ $producto->nombre }}</span>
            </div>
        </div>

        <div class="master-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12  col-md-9  col-md-push-3" role="main">
                        <div class="product product-type-simple">
                            <div class="images">
                                <div id="primary-slider" class="splide">
                                    <div class="splide__track">
                                        <ul class="splide__list">
                                            @foreach($arreglo_json as $imagen)
                                                <li class="splide__slide">
                                                    <img src="{{ asset('/storage/'.$imagen) }}">
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                                <div id="secondary-slider" class="splide">
                                    <div class="splide__track">
                                        <ul class="splide__list">
                                            @foreach($arreglo_json as $imagen)
                                                <li class="splide__slide">
                                                    <img src="{{ asset('/storage/'.$imagen) }}">
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="summary entry-summary">
                                <h1 class="product_title entry-title">{{ $producto->nombre }}</h1>
                                <div class="short-description" style="color: #000000;">
                                    <p>
                                        <strong>Características:</strong>
                                        {{ $producto->caracteristicas }}
                                    </p>
                                    <br>
                                    <p>
                                        <strong> Colores disponibles:</strong>
                                        {!! $producto->colores !!}
                                    </p>
                                    <br>
                                    <strong>Presentaciones disponibles</strong> : <br>
                                    {{ $producto->presentaciones }}

                                </div>
                                <div class="product_meta">
                                    <span class="posted_in">Categoría <a href="{{ route('subcategorias-didactico') }}">Didácticos y Artísticos</a>.</span>
                                </div>
                            </div><!-- .summary -->

                            <div class="woocommerce-tabs">
                                <div class="panel entry-content" id="tab-description" style="color: #000000;">
                                    <h2>Descripción</h2>
                                    <p>
                                        <strong>Embalaje</strong> : <br>
                                        {{ $producto->embalaje }}
                                        <br>
                                        <br>
                                        <strong>Usos: </strong> <br>
                                        {{ $producto->usos }}
                                        <br>
                                        <br>
                                        <strong>Precauciones: </strong> <br>
                                        {{ $producto->precauciones }}
                                    </p>
                                </div>
                            </div>

                            <div class="related products productos-relacionados">
                                <h2>Productos relacionados</h2>
                                <ul class="products">
                                    <li class="product first lista">
                                        <a href="{{ route('producto.show', 3) }}">
                                            <img width="150" height="150"
                                                 src="{{ asset('images/productos-jorge/tempera.png') }}"
                                                 alt="Brush"/>
                                            <h3>Témperas</h3>

                                        </a>
                                    </li>

                                    <li class="product lista">
                                        <a href="{{ route('producto.show', 5) }}">
                                            <img width="150" height="150"
                                                 src="{{ asset('images/productos-jorge/masa_suave_morado.png') }}" alt="Hammer"/>
                                            <h3>Masa Suave</h3>

                                        </a>

                                    </li>
                                    <li class="product last lista">
                                        <a href="{{ route('producto.show', 2) }}">
                                            <img width="150" height="150"
                                                 src="{{ asset('images/productos-jorge/crayones.png') }}"
                                                 alt="Hand Tools"/>
                                            <h3>Crayones</h3>

                                        </a>

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12  col-md-3  col-md-pull-9">
                        @include('productos.partials.busqueda-categorias')
                    </div>
                </div>
            </div>
        </div>
        @include('index-partials.footer')
    </div>
@endsection

@section('extra-js')
    <script src="{{ asset('js/splide.min.js')}}"></script>

    <script>
        // Create and mount the thumbnails slider.
        var secondarySlider = new Splide('#secondary-slider', {
            rewind: true,
            fixedWidth: 80,
            fixedHeight: 80,
            isNavigation: true,
            gap: 10,
            focus: 'center',
            pagination: false,
            cover: true,
            breakpoints: {
                '600': {
                    fixedWidth: 66,
                    fixedHeight: 40,
                }
            }
        }).mount();

        // Create the main slider.
        var primarySlider = new Splide('#primary-slider', {
            type: 'fade',
            heightRatio: 0.8,
            pagination: false,
            arrows: false,
            cover: true,

        });

        // Set the thumbnails slider as a sync target and then call mount.
        primarySlider.sync(secondarySlider).mount();

    </script>
@endsection


