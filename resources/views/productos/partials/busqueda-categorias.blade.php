<div class="sidebar shop-sidebar">
    <!-- Search Widget -->
    <div class="widget woocommerce widget_product_search push-down-30">
        <h4 class="sidebar__headings">Buscar Productos:</h4>
        <form method="POST" action="{{ route('buscar.store') }}" id="searchform" class="woocommerce-product-search">
            {{ csrf_field() }}
            <div>
                <label class="screen-reader-text" for="s">Buscar</label>
                <input type="text" name="busqueda" id="s" placeholder="Buscar producto"/>
                <input type="submit" id="searchsubmit" value="Buscar"/>
            </div>
        </form>
    </div>

    <!-- Product categories Widget -->
    <div class="widget woocommerce widget_product_categories push-down-30">
        <h4 class="sidebar__headings">Categorías</h4>
        <ul class="product-categories">
            <li><a href=" {{ route('construccion.index') }}">Construcción</a></li>
            <li><a href="{{ route('limpieza.index') }}">Limpieza y Desinfección</a></li>
            <li><a href="{{ route('categoria.index', 'escolar') }}">Escolar</a></li>
            <li><a href="{{ route('categoria.index', 'manualidades') }}">Manualidades</a></li>
            <li><a href="{{ route('categoria.index', 'fantasia') }}">Fantasía</a></li>
        </ul>
    </div>

</div>
