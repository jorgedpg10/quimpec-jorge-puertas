<div class="top">
    <div class="container">
        <div class="row">
            <div class="col-xs-12  col-md-6">
                <div class="top__left">Buenas ideas.. Buena Química.. Buenos Productos</div>
            </div>
            <div class="col-xs-12  col-md-6">
                <div class="top__right">

                </div>
            </div>
        </div>
    </div>
</div>
<header class="header">
    <div class="container">
        <div class="logo">
            <a href="{{ route('index') }}">
                <img src=" {{ asset('images/logo.png') }} " alt="logo quimpec" class="img-responsive" width="50%"/>
            </a>
        </div>
        <div class="header-widgets  header-widgets-desktop">
            <div class="widget  widget-icon-box">
                <div class="icon-box">
                    <i class="fa  fa-phone  fa-3x"></i>
                    <div class="icon-box__text">
                        <h4 class="icon-box__title">+593 02 2975570</h4>
                        <span class="icon-box__subtitle">ventas@quimpec.com</span>
                    </div>
                </div>
            </div>
            <div class="widget  widget-icon-box">
                <div class="icon-box">
                    <i class="fa fa-location-arrow fa-3x"></i>
                    <div class="icon-box__text">
                        <h4 class="icon-box__title">Sector Parque Industrial de Turubamba</h4>
                        <span class="icon-box__subtitle">Calle A Lote 37 y Calle J</span><br>
                        <span class="icon-box__subtitle">Quito - Ecuador</span>
                    </div>
                </div>
            </div>
            <div class="widget  widget-icon-box">
                <div class="icon-box">
                    <i class="fa  fa-clock-o  fa-3x"></i>
                    <div class="icon-box__text">
                        <h4 class="icon-box__title">Lun-Vie 8.00 - 16.30</h4>
                        {{--<span class="icon-box__subtitle">8.00 - 18.00</span>--}}
                    </div>
                </div>
            </div>
            <div class="widget  widget-social-icons">
                <a class="social-icons__link" href="https://www.facebook.com/quimpec" target="_blank"><i class="fa  fa-facebook"></i></a>
                <a class="social-icons__link" href="https://www.instagram.com/deditos_ec" target="_blank"><i class="fa  fa-instagram"></i></a>
            </div>
        </div>
        <!-- Toggle Button for Mobile Navigation -->
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#buildpress-navbar-collapse">
            <span class="navbar-toggle__text">MENU</span>
            <span class="navbar-toggle__icon-bar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</span>
        </button>
    </div>
    <div class="sticky-offset  js-sticky-offset"></div>
    <div class="container">
        <div class="navigation">
            <div class="collapse  navbar-collapse" id="buildpress-navbar-collapse">
                <ul id="menu-main-menu" class="navigation--main">
                    <ul id="menu-main-menu" class="navigation--main">
                        <li class="current-menu-item"><a href="{{ route('index') }}">HOME</a></li>
                        <li><a href=" {{ route('index-scroll.vista', 'quienes-somos') }}">QUIENES SOMOS</a></li>
                        <li><a href="{{ route('categorias') }}">CATÁLOGO</a></li>
                        <li><a href="{{ route('index-scroll.vista', 'proyectos') }}">PROYECTOS</a></li>
                        <li><a href="{{ route('blog') }}">BLOG</a></li>

                        <li><a href="{{ route('contacto') }}">CONTACTO</a></li>
                    </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="header-widgets  hidden-md  hidden-lg">
            <div class="widget  widget-icon-box">
                <div class="icon-box">
                    <i class="fa  fa-phone  fa-3x"></i>
                    <div class="icon-box__text">
                        <h4 class="icon-box__title">+593 02 2975570</h4>
                        <span class="icon-box__subtitle">ventas@quimpec.com</span>
                    </div>
                </div>
            </div>
            <div class="widget  widget-icon-box">
                <div class="icon-box">
                    <i class="fa fa-location-arrow fa-3x"></i>
                    <div class="icon-box__text">
                        <h4 class="icon-box__title">Sector Parque Industrial de Turubamba</h4>
                        <span class="icon-box__subtitle">Calle A Lote 37 y Calle J</span><br>
                        <span class="icon-box__subtitle">Quito - Ecuador</span>
                    </div>
                </div>
            </div>
            <div class="widget  widget-icon-box">
                <div class="icon-box">
                    <i class="fa  fa-clock-o  fa-3x"></i>
                    <div class="icon-box__text">
                        <h4 class="icon-box__title">Lun-Vie 8.00 - 16.30</h4>

                    </div>
                </div>
            </div>
            <div class="widget  widget-social-icons">
                <a class="social-icons__link" href="https://www.facebook.com/quimpec" target="_blank"><i class="fa fa-facebook"></i></a>
                <a class="social-icons__link" href="https://www.instagram.com/deditos_ec" target="_blank"><i class="fa fa-instagram"></i></a>

            </div>
        </div>
    </div>
</header>
