@extends('layouts.app')

@section('title', 'Catálogo')
@section('clase-body', 'woocommerce-page')

@section('extra-css')
    <link rel="stylesheet" href="{{ asset('css/shop.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/categorias.css') }}"/>

@endsection

@section('content')
    <div class="boxed-container">
        @include('partials.navbar')

        <div class="main-title">
            <div class="container">
                <h1 class="main-title__primary">Catálogo</h1>
                <h3 class="main-title__secondary">NUESTROS PRODUCTOS</h3>
            </div>
        </div>
        <div class="breadcrumbs ">
            <div class="container">
                <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to BuildPress."
                                               href="index.html" class="home">BuildPress</a></span>
                <span property="v:title">Shop</span>
            </div>
        </div>
        <div class="master-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12  col-md-9  col-md-push-3" role="main">
                        <div class="clearfix">
                            <p class="woocommerce-result-count">Showing 1&ndash;12 of 18 results</p>
                            <form class="woocommerce-ordering" method="get">
                                <select name="orderby" class="orderby">
                                    <option value="popularity">Sort by popularity</option>
                                    <option value="rating">Sort by average rating</option>
                                    <option value="date">Sort by newness</option>
                                    <option value="price">Sort by price: low to high</option>
                                    <option value="price-desc">Sort by price: high to low</option>
                                </select>
                            </form>
                        </div>


                        <div class="grid-container">
                            <div class="item">
                                <div class="product-image">
                                    <a href="{{ route('plastilina') }}">
                                        <img width="150" height="150"
                                             src="{{ asset('images/artisticos/plastilina-grupo.png') }}" alt="Brush"/>
                                    </a>
                                </div>

                                <div class="product-title">
                                    <a href="{{ route('plastilina') }}">
                                        <h3>Plastilina</h3>
                                        <p class="nombre-catalogo">plastilina</p>
                                    </a>
                                </div>

                            </div>

                            <div class="item">

                                <div class="product-image">
                                    <a href="{{ route('plastilina') }}">
                                        <img width="150" height="150" src="{{ asset('images/artisticos/crayones-grupo.png') }}"
                                             alt="Forceps"/>
                                    </a>
                                </div>

                                <div class="product-title">
                                    <a href="{{ route('plastilina') }}">
                                        <h3>Crayones</h3>
                                        <p class="nombre-catalogo">Crayones</p>
                                    </a>
                                </div>

                            </div>
                            <div class="item">
                                <a href="shop-single.html">
                                    <img width="150" height="150" src="{{ asset('images/artisticos/tempera.png') }}"
                                         alt="Forceps"/>
                                    <h3>Témperas</h3>
                                    <p class="nombre-catalogo">Témperas</p>

                                </a>
                            </div>
                            <div class="item">
                                <a href="shop-single.html">
                                    <img width="150" height="150" src="{{ asset('images/artisticos/arcilla.png') }}"
                                         alt="Forceps"/>
                                    <h3>Arcilla para Moldear</h3>
                                    <p class="nombre-catalogo">Arcilla para Moldear</p>
                                </a>

                            </div>
                            <div class="item">
                                <a href="shop-single.html">
                                    <img width="150" height="150" src="{{ asset('images/artisticos/masa-grupo.png') }}"
                                         alt="Forceps"/>
                                    <h3>Masa Suave para Moldear</h3>
                                    <p class="nombre-catalogo">Masa Suave para Moldear</p>
                                </a>

                            </div>
                            <div class="item">
                                <a href="shop-single.html">
                                    <img width="150" height="150" src="{{ asset('images/artisticos/pinta-dedos-grupo.png') }}"
                                         alt="Forceps"/>
                                    <h3>Pinta Dedos</h3>
                                    <p class="nombre-catalogo">Pinta Dedos</p>
                                </a>
                            </div>
                        </div>

                        <div class="item">
                            <a href=" {{ route('pinta-dedos') }}">
                                <img width="150" height="150" src="{{ asset('images/artisticos/body-paint.png') }}"
                                     alt="Forceps"/>
                                <h3>Body Paint</h3>
                                <p class="nombre-catalogo">Body Paint</p>
                            </a>
                        </div>

                        {{-- <nav class="pagination text-center">
                             <ul class="page-numbers">
                                 <li><span class="page-numbers current">1</span></li>
                                 <li><a class="page-numbers" href="#">2</a></li>
                                 <li><a class="next page-numbers" href="#"><i class="fa fa-caret-right"></i></a></li>
                             </ul>
                         </nav>--}}
                    </div>
                    <div class="col-xs-12  col-md-3  col-md-pull-9">
                        <div class="sidebar shop-sidebar">
                            <!-- Search Widget -->
                            <div class="widget woocommerce widget_product_search push-down-30">
                                <h4 class="sidebar__headings">Search Products</h4>
                                <form method="get" id="searchform" class="woocommerce-product-search">
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="s" id="s" placeholder="Search for products"/>
                                        <input type="submit" id="searchsubmit" value="Search"/>
                                        <input type="hidden" name="post_type" value="product"/>
                                    </div>
                                </form>
                            </div>

                            @include('partials-catalogo.menu-categorias')

                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('partials.footer')
    </div>

@endsection
