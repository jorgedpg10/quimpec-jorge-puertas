@extends('layouts.app')

@section('title', $post->title )

@section('clase-body', 'home page')
@section('content')

    <div class="boxed-container">
        @include('partials.navbar')

        <div class="main-title" style="background-color: #f2f2f2; ">
            <div class="container">
                <h1 class="main-title__primary">Blog</h1>
                <h3 class="main-title__secondary">Novedades en nuestra empresa</h3>
            </div>
        </div>
        <div class="breadcrumbs ">
            <div class="container">
                <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Quimpec"
                                               href="{{ route('index') }}" class="home">Quimpec</a></span>
                <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="ir al Blog." href="{{ route('blog') }}">Blog</a></span>
                <span typeof="v:Breadcrumb"><span property="v:title">{{ $post->title }}</span></span>
            </div>
        </div>

        <div class="master-container">
            <div class="container">
                <div class="row">
                    <main class="col-xs-12  col-md-9" role="main">
                        <div class="row">

                            <div class="col-xs-12">
                                <article class="post hentry post-inner">
                                    <a href="blog-single.html">
                                        <img width="848" height="480" src="{{ asset('/storage/'.$post->image) }}"
                                             class="img-responsive wp-post-image" alt="Content Image"/>
                                    </a>
                                    <div class="meta-data">
                                        <span>{{ $post->created_at->format('d M Y') }}</span>
                                        <span class="meta-data__author">Por Jorge Puertas</span>
                                    </div>
                                    <h2 class="hentry__title">
                                        <a href="blog-single.html">{{ $post->title}}</a>
                                    </h2>
                                    <div class="hentry__content">
                                        {!! $post->body !!}
                                    </div>
                                    <div class="clearfix"></div>
                                </article>
                            </div><!-- /blogpost -->

                        </div>
                    </main>
                    <div class="col-xs-12  col-md-3">
                        <div class="sidebar">
                            @include('blog.partials.posts-relacionados')
                        </div>
                    </div>
                </div>
            </div><!-- /container -->
        </div>
        @include('index-partials.footer')
    </div>
@endsection
