<div class="widget  widget_search  push-down-30">
    <form role="search" method="get" class="search-form">
        <label>
            <span class="screen-reader-text">Search for:</span>
            <input type="search" class="search-field" placeholder="Search &hellip;" value=""
                   name="s" title="Buscar Post..."/>
        </label>
        <input type="submit" class="search-submit" value="Buscar"/>
    </form>
</div>
<div class="widget widget_recent_entries push-down-30">
    <h4 class="sidebar__headings">Otros Posts</h4>
    <ul>
        <li><a href="{{ route('blog.show', 1) }}">Ayuda social de Quimpec</a></li>
    </ul>
</div>
