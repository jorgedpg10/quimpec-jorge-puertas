<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8"/>
    <title>Quimpec Químicas - @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    @yield('seo')
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro%3A400,700%3Alatin%7CMontserrat%3A700%3Alatin"/>
    <link rel="stylesheet" href=" {{ asset('css/style.css') }} "/>
    <link rel="stylesheet" href=" {{ asset('css/prettyPhoto.css') }} "/>
    <link rel="stylesheet" href=" {{ asset('css/font-awesome.css') }} "/>
    @yield('extra-css')

    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('js/isotope.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('js/imagesloaded.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('js/modernizr.custom.24530.js') }}" ></script>

</head>
<body class="@yield('clase-body')">
    @yield('content')

<script src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/almond.js') }} "></script>
<script type="text/javascript" src=" {{ asset('js/underscore.js') }}"></script>
<script type="text/javascript" src=" {{ asset('js/jquery.prettyPhoto.js') }}"></script>
<script type="text/javascript" src=" {{ asset('js/header_carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/sticky_navbar.js') }} "></script>
<script type="text/javascript" src="{{ asset('js/simplemap.js') }} "></script>
<script type="text/javascript" src="{{asset('js/main.min.js')}} "></script>
<script type="text/javascript" src="{{ asset('js/main.js') }} "></script>
@yield('extra-js')
</body>
</html>
