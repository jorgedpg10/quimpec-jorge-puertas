@extends('layouts.app')

@section('title', 'Contacto')

@section('extra-css')
    <link rel="stylesheet" href="{{ asset('css/blog.css') }}"/>
@endsection

@section('clase-body', 'home page')
@section('content')

    <div class="boxed-container">
        @include('partials.navbar')

        <div class="main-title" style="background-color: #f2f2f2; ">
            <div class="container">
                <h1 class="main-title__primary">Contacto</h1>
                <h3 class="main-title__secondary">Novedades en nuestra empresa</h3>
            </div>
        </div>
        <div class="breadcrumbs ">
            <div class="container">
                <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to BuildPress."
                                               href="{{ route('index') }}" class="home">Quimpec</a></span>
                <span typeof="v:Breadcrumb"><span property="v:title">Contacto</span></span>
            </div>
        </div>

        <div class="master-container">
            <div class="hentry container" role="main">
                <div class="row">
                    <div class="col-md-12">

                        <div class="spacer"></div>
                        <div class="row">
                            <div class="col-md-6 textwidget">
                                <span class="icon-container"><span class="fa fa-phone"></span></span> <b>+593 02 2975570</b><br>
                                <span class="icon-container"><span class="fa fa-envelope"></span></span> <a href="mailto: ventas@quimpec.com"> ventas@quimpec.com</a>
                            </div>
                            <div class="col-md-6 textwidget">
                                <span class="icon-container"><span class="fa fa-location-arrow"></span></span> <b>Sector Parque Industrial de Turubamba</b><br>
                                Calle A Lote 37 y Calle J<br>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="spacer"></div>

            </div><!-- /container -->
        </div>


        @include('index-partials.footer')
    </div>
@endsection
