<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8"/>
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <meta name="title" content="Construcción - Material didáctico artístico - Limpieza Quito Ecuador"/>
    <meta name="description" content="Techos acústicos, Pintura acústica, Pintura para cara, Body paint,
Impermeabilizante de humedad, Jabón líquido y alcohol en gel, Limpiavidrios y limpia tapiceria, Pintura para tela,
plastilinas, crayones, temperas, arcilla, masa suave, Maquillaje de fantasía" />
    <meta name="keywords" content="Techos acústicos, Pintura acústica, Pintura para cara, Body paint,
Impermeabilizante de humedad, Jabón líquido y alcohol en gel, Limpiavidrios y limpia tapiceria, Pintura para tela,
plastilinas, crayones, temperas, arcilla, masa suave, Maquillaje de fantasía" />

    <meta http-equiv="content-language" content="es">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro%3A400,700%3Alatin%7CMontserrat%3A700%3Alatin"/>

    <link rel="stylesheet" href=" {{ asset('css/style.css') }} "/>
    <link rel="stylesheet" href=" {{ asset('css/prettyPhoto.css') }} "/>
    <link rel="stylesheet" href=" {{ asset('css/font-awesome.css') }} "/>
    <link rel="stylesheet" href=" {{ asset('css/servicios.css') }} "/>

    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/isotope.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/imagesloaded.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/modernizr.custom.24530.js') }}"></script>
</head>
<body class="home page boxed">

<div class="boxed-container">
    @include('index-partials.navbar')

    @include('index-partials.hero')
    <div class="master-container">
        <div class="promo">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="siteorigin-panels-stretch panel-row-style" style="background-color:#eeeeee">
                            <div class="panel-grid-cell">
                                <div class="panel widget widget_pt_banner panel-first-child panel-last-child">
                                    <div class="banner__text">
                                        Busca soluciones de calidad para sus proyectos?
                                    </div>
                                    <div class="banner__buttons">
                                        <a class="btn btn-primary" onclick="scrollProyectos()"
                                           target="_self">PROYECTOS</a>&nbsp;
                                        <a class="btn  btn-default" href="{{ route('categorias') }}" target="_self">CATÁLOGO</a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('index-partials.servicios')

        <div class="spacer-big"></div>

        @include('index-partials.mision')
        <div class="spacer-big"></div>

        @include('index-partials.testimonios')
        <div class="panel-grid" id="pg-7-5">
            <div class="promobg">
                <div class="container">
                    <div class="panel widget row">
                        <div class="col-md-12">
                            <div class="motivational-text">
                                Fabricamos productos para cubrir las necesidades en varias áreas de la industria y
                                ofrecer productos manufacturados con elevado valor agregado.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="spacer-big"></div>

        @include('index-partials.clientes')
    </div>

    @include('index-partials.footer')
</div><!-- end of .boxed-container -->

<script type="text/javascript" src="{{ asset('js/almond.js') }} "></script>
<script type="text/javascript" src=" {{ asset('js/underscore.js') }}"></script>
<script type="text/javascript" src=" {{ asset('js/jquery.prettyPhoto.js') }}"></script>
<script type="text/javascript" src=" {{ asset('js/header_carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/sticky_navbar.js') }} "></script>
<script type="text/javascript" src="{{ asset('js/simplemap.js') }} "></script>
<script type="text/javascript" src="{{asset('js/main.min.js')}} "></script>
<script type="text/javascript" src="{{ asset('js/main.js') }} "></script>
<script type="text/javascript" src="{{ asset('js/require.js') }} "></script>

</body>

<script>
    function scrollQuienesSomos() {
        var elem = document.getElementById("quienes-somos");
        elem.scrollIntoView({behavior: "smooth"});
    }

    function scrollProyectos() {
        var elem = document.getElementById("proyectos");
        elem.scrollIntoView({behavior: "smooth"});
    }

    $( document ).ready(function() {
        var posicion = {!! json_encode($element) !!};

        if( posicion != 0){
            var el = document.getElementById(posicion);
            el.scrollIntoView({behavior: "smooth"});
        }
    });


</script>

</html>
