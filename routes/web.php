<?php

use App\Http\Controllers\BuscarController;
use App\Http\Controllers\ConstruccionController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\LimpiezaController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\ScrollController;
use Illuminate\Support\Facades\Route;

Route::get('/', [IndexController::class, 'index'])->name('index');

Route::get('/quienes-somos', function () {
    return view('quienes-somos');
})->name('quienes-somos');

Route::get('/catalogo', function () {
    return view('productos.categorias');
})->name('categorias');

Route::get('/escolar-artistico', function () {
    return view('productos.subcategorias-didactico');
})->name('subcategorias-didactico');

Route::get('/contacto', function () {
    return view('contacto');
})->name('contacto');


Route::get('/landing/fantasia', function () { return view('landing'); })->name('landing');

Route::get('/productos-construccion', [ConstruccionController::class, 'index'])->name('construccion.index');
Route::get('/productos-construccion/{product}', [ConstruccionController::class, 'show'])->name('producto-construccion.show'); // /producto-construccion/1

Route::get('/productos-limpieza', [LimpiezaController::class, 'index'])->name('limpieza.index');
Route::get('/productos-limpieza/{product}', [LimpiezaController::class, 'show'])->name('producto-limpieza.show'); // /producto-construccion/1

Route::get('/index-scroll/{elem}', [ScrollController::class, 'vista'])->name('index-scroll.vista'); // /producto-construccion/1

Route::get('/categorias/{categoria}', [ProductoController::class, 'index'])->name('categoria.index'); // /categoria/escolar

Route::get('/productos/{product}', [ProductoController::class, 'show'])->name('producto.show'); // /product/1

Route::get('/proyectos', function () { return view('proyectos'); })->name('proyectos');

Route::get('/blog', [PostController::class, 'index'])->name('blog');
Route::get('/blog/{blog}', [PostController::class, 'show'])->name('blog.show'); // /blog/1

Route::post('/buscar', [BuscarController::class, 'store'])->name('buscar.store');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
