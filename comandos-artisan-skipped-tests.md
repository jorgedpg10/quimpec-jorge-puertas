#Comandos artisan
 
para limpiar cache de las vistas
php artisan cache:clear
php artisan view:clear

para que tome cambios del .env
php artisan config:clear

Para correr migraciones y seeders en db de testing:
php artisan migrate:fresh --seed  --env=testing  (la conexión se define en .env.testing)

correr un seeder en específico
php artisan db:seed --class=UsersTableSeeder

versión de Laravel
php artisan --version

#Comandos Composer
Para reconocer clases que el proyecto no encuentra:
composer dump-autoload

#Comandos PhpUnit
para filtrar por metodo

vendor/bin/phpunit --filter se_crea_una_nueva_venta

para saltarse una prueba:
$this->markTestSkipped( 'prueba no terminada');

#Comandos Dusk
hacer prueba:
php artisan dusk:make LoginTest

filtrar prueba: 
php artisan dusk tests/browser/AuthTest.php
