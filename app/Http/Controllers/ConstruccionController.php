<?php

namespace App\Http\Controllers;

use App\Models\ProductoConstruccion;
use Illuminate\Http\Request;

class ConstruccionController extends Controller
{
    public function index() {

        $productos = ProductoConstruccion::all();

        return view('productos.construccion.index')->with([
            'productos' => $productos
        ]);

    }


    public function show($id)
    {
        $producto = ProductoConstruccion::findOrFail($id);
        $arreglo_json = json_decode($producto->imagenes_individuales);

        return view('productos.construccion.product')->with([
            'producto' => $producto,
            'arreglo_json' => $arreglo_json
        ]);
    }
}
