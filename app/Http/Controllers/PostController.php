<?php

namespace App\Http\Controllers;

use App\Models\QuimpecPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use TCG\Voyager\Models\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return View::make('blog.index', ['posts' => $posts]);

    }

    public function show($id){
        $post = Post::findOrfail($id);

        return View::make('blog.show', ['post' => $post]);
    }
}
