<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use App\Models\ProductoConstruccion;
use App\Models\ProductoLimpieza;
use Illuminate\Http\Request;

class BuscarController extends Controller
{
    public function store(Request $request){

        $productos_didacticos = Producto::where('nombre', 'like', "%$request->busqueda%")->get();
        $productos_construccion = ProductoConstruccion::where('nombre', 'like', "%$request->busqueda%")->get();
        $productos_limpieza = ProductoLimpieza::where('nombre', 'like', "%$request->busqueda%")->get();

        $productos_acum = $productos_didacticos->merge($productos_construccion);
        $productos = $productos_acum->merge($productos_limpieza);

        return view('busqueda-productos')->with('productos', $productos);
    }
}
