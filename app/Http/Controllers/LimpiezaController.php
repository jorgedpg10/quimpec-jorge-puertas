<?php

namespace App\Http\Controllers;

use App\Models\ProductoLimpieza;
use Illuminate\Http\Request;

class LimpiezaController extends Controller
{
    public function index() {

        $productos = ProductoLimpieza::all();

        return view('productos.limpieza.index')->with([
            'productos' => $productos
        ]);

    }

    public function show($id)
    {
        $producto = ProductoLimpieza::findOrFail($id);
        $arreglo_json = json_decode($producto->imagenes_individuales);

        return view('productos.limpieza.product')->with([
            'producto' => $producto,
            'arreglo_json' => $arreglo_json
        ]);
    }
}
