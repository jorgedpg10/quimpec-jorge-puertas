<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use App\Models\ProductoConstruccion;
use Illuminate\Http\Request;

class ProductoController extends Controller {
    public function index($categoria) {

        $productos = Producto::where('categoria', $categoria)->get();

        return view('productos.didactico.index')->with([
            'productos' => $productos
        ]);
    }


    public function show($id) {
        $producto = Producto::findOrFail($id);
        $arreglo_json = json_decode($producto->imagenes_individuales);

        return view('productos.didactico.product')->with([
            'producto' => $producto,
            'arreglo_json' => $arreglo_json
        ]);
    }

}
