<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ScrollController extends Controller
{
    public function vista($elem){
        return view('index')->with([
            'element' => $elem
        ]);
    }
}
