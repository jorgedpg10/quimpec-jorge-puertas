<?php

namespace Database\Factories;

use App\Models\QuimpecPost;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuimpecPostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = QuimpecPost::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'author_id' =>  $this->faker->randomNumber($nbDigits = 1, $strict = false),
            'category_id' =>$this->faker->randomNumber($nbDigits = 1, $strict = false),
            'title' => $this->faker->sentence(),
            'body' => $this->faker->paragraph(),
            'slug' => $this->faker->slug(),

        ];
    }
}
