<?php

namespace Database\Seeders;

use App\Models\QuimpecPost;
use App\Models\User;
use Illuminate\Database\Seeder;


class QuimpecPostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->make([
           'name' => 'Jorge'
        ]);
        QuimpecPost::factory()->make();

    }
}
