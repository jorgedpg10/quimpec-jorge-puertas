<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableProductosLimpieza extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos_limpieza', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->unique();
            $table->text('texto1')->nullable();
            $table->text('texto2')->nullable();
            $table->text('imagen_portada')->nullable();
            $table->text('imagenes_individuales')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos_limpieza');
    }
}
