<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterColumnsEmbalajeUsosLength extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('productos', function (Blueprint $table) {
            $table->string('embalaje', 500)->nullable()->change();
            $table->string('usos', 700)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('productos', function (Blueprint $table) {
            $table->string('embalaje', 255)->nullable()->change();
            $table->string('usos', 500)->nullable()->change();
        });

    }
}
