<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->unique();
            $table->string('categoria');
            /*$table->string('slug')->unique();*/
            $table->string('caracteristicas', 500)->nullable();
            $table->string('colores')->nullable();
            $table->string('presentaciones')->nullable();
            $table->string('embalaje')->nullable();
            $table->string('usos', 500)->nullable();
            $table->string('precauciones', 300)->nullable();
            $table->text('imagen_portada')->nullable();
            $table->text('imagenes_individuales')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
